<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResidenceInsertRequest;
use App\Http\Requests\ResidenceListRequest;
use App\Models\Residence;
use App\Repositories\Repository;
use App\Repositories\ResidenceRepository;
use Illuminate\Http\JsonResponse;

class ResidenceController extends Controller
{

    public $model;
    public $model_repository;
    public $residence_repository;

    public function __construct()
    {
        $model = new Residence();
        $model_repository = new Repository();
        $residence_repository = new ResidenceRepository();
        $this->model = $model;
        $this->model_repository = $model_repository;
        $this->residence_repository = $residence_repository;
    }

    public function index(ResidenceListRequest $request): JsonResponse
    {
        $result = $this->residence_repository->getFulfilled($request);
        return response()->json([
            'status' => 'OK',
            'result' => $result
        ]);
    }

    public function store(ResidenceInsertRequest $request): JsonResponse
    {
        $this->model_repository->create($this->model, $request->all());
        return response()->json([
            'status' => 'OK'
        ]);
    }
}

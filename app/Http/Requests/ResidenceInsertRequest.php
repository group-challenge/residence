<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResidenceInsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'slug' => 'required|string|max:255',
            'name_fa' => 'required|string|max:255',
            'capacity' => 'required|integer|max:11',
            'is_reserved' => 'required|boolean',
            'cost_id' => 'required|exists:App\Models\Cost,id',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResidenceListRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'slug' => 'nullable|string|max:255',
            'name_fa' => 'nullable|string|max:255',
            'capacity' => 'nullable|integer|max:11',
            'is_reserved' => 'nullable|boolean',
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Cost extends Model
{
    use HasFactory;

    protected $table = 'costs';

    protected $fillable = ['base_price', 'extra_price_id','created_at', 'updated_at'];

    protected $guarded = ['id'];

    const UPDATED_AT = null;

    /**
     * Get the extra_price associated with the cost.
     */
    public function extraPrice()
    {
        return $this->belongsTo(ExtraPrice::class);
    }
    /**
     * Get the cost associated with the residence.
     */
    public function residence(): HasOne
    {
        return $this->hasOne(Residence::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ExtraPrice extends Model
{
    use HasFactory;

    protected $table = 'extra_prices';

    protected $fillable = ['type', 'percent', 'created_at', 'updated_at'];

    protected $guarded = ['id'];

    const UPDATED_AT = null;

    public function cost(): HasOne
    {
        return $this->hasOne(Cost::class);
    }
}

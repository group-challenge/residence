<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Residence extends Model
{
    use HasFactory;

    protected $table = 'residences';

    protected $fillable = ['slug', 'name_fa', 'is_reserved', 'cost_id', 'capacity', 'created_at', 'updated_at'];

    protected $guarded = ['id'];

    const UPDATED_AT = null;

    /**
     * Get the cost associated with the residence.
     */
    public function cost(): BelongsTo
    {
        return $this->belongsTo(Cost::class);
    }
}

<?php

namespace App\Repositories;

use App\Models\Residence;

class ResidenceRepository
{
    public function getFulfilled($request)
    {
        return Residence::when($request->input('slug'), function ($query) use ($request) {
            return $query->where('slug', 'like', '%' . $request->input('slug') . '%');
        })
            ->when($request->input('name_fa'), function ($query) use ($request) {
                return $query->where('name_fa', 'like', '%' . $request->input('name_fa') . '%');
            })
            ->when($request->input('capacity'), function ($query) use ($request) {
                return $query->where('capacity', $request->input('capacity'));
            })
            ->when($request->input('is_reserved') === 0 || 1, function ($query) use ($request) {
                return $query->where('is_reserved', $request->input('is_reserved'));
            })
            ->orderby('slug')
            ->get();
    }
}
<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CostFactory extends Factory
{
    public function definition(): array
    {
        $extra_price = \App\Models\ExtraPrice::select('id')->inRandomOrder()->get()->toArray();

        return [
            'base_price' => random_int(100,1000),
            'extra_price_id' => $extra_price[array_rand($extra_price)]['id'],
            'created_at' => now()
        ];
    }
}

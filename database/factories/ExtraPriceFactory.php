<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class ExtraPriceFactory extends Factory
{
    public function definition(): array
    {
        $array = ['baby', 'child', 'adult'];
        return [
            'type' => $array[array_rand($array)],
            'percent' => random_int(1,100),
            'created_at' => now()
        ];
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ResidenceFactory extends Factory
{
    public function definition(): array
    {
        $cost_id = \App\Models\Cost::select('id')->inRandomOrder()->get()->toArray();

        return [
            'slug' => fake()->name(),
            'name_fa' => fake()->name(),
            'capacity' => random_int(1, 10),
            'is_reserved' => fake()->boolean,
            'cost_id' => $cost_id[array_rand($cost_id)]['id'],
            'created_at' => now()
        ];
    }
}

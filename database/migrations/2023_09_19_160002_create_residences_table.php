<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('residences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name_fa');
            $table->boolean('is_reserved');
            $table->integer('cost_id')->unsigned()->index('cost_id')->nullable();
            $table->integer('capacity');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('cost_id', 'cost_id')
                ->references('id')
                ->on('costs');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('residences');
    }
};
